golang-github-fluent-fluent-logger-golang (1.9.0-1+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Wed, 22 Jan 2025 14:17:33 +0100

golang-github-fluent-fluent-logger-golang (1.9.0-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 10:42:22 +0000

golang-github-fluent-fluent-logger-golang (1.9.0-1) unstable; urgency=medium

  * Team upload.

  [ Dmitry Smirnov ]
  * DH-compat

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Tianon Gravi ]
  * Remove self from Uploaders

  [ Arnaud Rebillout ]
  * New upstream version 1.9.0
  * Modernize packaging files
  * Bump standards version (no change needed)

  [ Shengjing Zhu ]
  * Remove test dependency from -dev package

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 31 Mar 2022 00:59:32 +0800

golang-github-fluent-fluent-logger-golang (1.5.0-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 19:49:17 +0000

golang-github-fluent-fluent-logger-golang (1.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Rules-Requires-Root: no.
  * Standards-Version: 4.5.0.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 29 Feb 2020 01:27:56 +1100

golang-github-fluent-fluent-logger-golang (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.4.0.
  * DH & compat to version 12.
  * Added myself to Uploaders.
  * Invoke "go generate" on build.
  * Build-Depends += "msgp".

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 12 Sep 2019 21:42:13 +1000

golang-github-fluent-fluent-logger-golang (1.3.0-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 11:52:36 +0000

golang-github-fluent-fluent-logger-golang (1.3.0-1) unstable; urgency=medium

  * Team upload.

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Dmitry Smirnov ]
  * New upstream release.
  * Testsuite: autopkgtest-pkg-go
  * debhelper & compat to version 11.
  * Standards-Version: 4.1.4; Priority: optional.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 11 Jun 2018 03:34:17 +1000

golang-github-fluent-fluent-logger-golang (1.1.0-2) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Team upload.
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL

  [ Konstantinos Margaritis ]
  * Replace golang-go with golang-any in Build-Depends, remove golang-go from
    Depends

 -- Konstantinos Margaritis <markos@debian.org>  Tue, 08 Aug 2017 19:51:11 +0300

golang-github-fluent-fluent-logger-golang (1.1.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release (Closes: #823469)
  * Update to standards version 3.9.8
  * Add docs, move examples into /usr/share/doc directory

 -- Tim Potter <tpot@hpe.com>  Tue, 17 May 2016 12:29:09 +1000

golang-github-fluent-fluent-logger-golang (1.0.0-1) unstable; urgency=medium

  * Initial release

 -- Tianon Gravi <tianon@debian.org>  Fri, 23 Oct 2015 15:19:45 -0700
